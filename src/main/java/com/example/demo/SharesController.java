package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.CompanySharesModel;

import ch.qos.logback.core.model.Model;

@Controller
public class SharesController {
	ArrayList<CompanySharesModel> sharesList = new ArrayList<CompanySharesModel>();

	@ModelAttribute
	public void modelData(ModelMap m) {
		m.addAttribute("sharesList", sharesList);
	}

	@RequestMapping("/")
	public String home() {
		
		
		//ModelAndView mv = new ModelAndView();
		//mv.setViewName("shares.jsp");
		return "shares";
	}



	@PostMapping("addNewShare")
	public String addNewShare(@ModelAttribute("share") CompanySharesModel s) {
		
		sharesList.add(s);
		//ModelAndView mv = new ModelAndView();
		//mv.setViewName("shares.jsp");

		return "redirect:/";
		//return "shares";
	}

	@GetMapping("loadNewCompanyForm")
	public String loadNewCompanyForm() {
		return "addNewShare";
	}
	
	@GetMapping("loadEditPage")
	public String loadShareEditForm(@RequestParam("Id")int id, ModelMap m) {
		for(int i = 0;i<sharesList.size();i++) {
			if(sharesList.get(i).getCompanyId() == id) {
				
				m.addAttribute("idToUpdate", sharesList.get(i).getCompanyId());
				m.addAttribute("nameToUpdate", sharesList.get(i).getCompanyName());
				m.addAttribute("priceToUpdate", sharesList.get(i).getSharePrice());
			}
		}
	
		return "editShareForm";
	}
	
	
	@PostMapping("updateSharePrice")
	public String updateSharePrice(@RequestParam("companyId")int id,CompanySharesModel s) {
		
		for(int i = 0;i<sharesList.size();i++) {
			if(sharesList.get(i).getCompanyId() == id) {
				
				sharesList.set(i, s);
			}
		}


		return "redirect:/";
		//return "shares";
	}
	
	
	
	@GetMapping("delete")
	public String deleteShare(@RequestParam("Id")int id) {
		for(int i = 0;i<sharesList.size();i++) {
			if(sharesList.get(i).getCompanyId() == id) {
				
				sharesList.remove(i);
			}
		}
		
		return "redirect:/";
		
	}


}
