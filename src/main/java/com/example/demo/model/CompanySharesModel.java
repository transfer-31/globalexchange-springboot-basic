package com.example.demo.model;

public class CompanySharesModel {
	
    private int companyId;
    private String companyName;
    private float sharePrice;
	public CompanySharesModel(int companyId, String companyName, float sharePrice) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.sharePrice = sharePrice;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public float getSharePrice() {
		return sharePrice;
	}
	public void setSharePrice(float sharePrice) {
		this.sharePrice = sharePrice;
	}
	@Override
	public String toString() {
		return "CompanySharesModel [companyId=" + companyId + ", companyName=" + companyName + ", sharePrice="
				+ sharePrice + "]";
	}
    
    

}
