<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.*,com.example.demo.model.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
	integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
	integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
	crossorigin="anonymous"></script>

</head>
</head>
<body align="center">
	<h1>welcome to Global Exchange</h1>
	<a href="loadNewCompanyForm">Enlist the new Company</a>
	<table class="table table-bordered ">
		<tr>
			<th>Company Id</th>
			<th>Company name</th>
			<th>Share price</th>
			<th colspan=2>Actions</th>
		</tr>

		
		<tr>
			<c:forEach var="share" items="${sharesList}">

				<td>${share.companyId}</td>
				<td>${share.companyName}</td>
				<td>${share.sharePrice}</td>
				<td><a href="loadEditPage?Id=${share.companyId}">edit</a></td>
				<td><a href="delete?Id=${share.companyId}">delete</a></td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>